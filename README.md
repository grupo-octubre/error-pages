# Actualización de repositorio

1. Darle permisos de ejecutable al archivo `gen-error-pages.sh` en caso de que no los tenga

2. Ejecutarlo para que descargar los archivos error*.html de producción

3. Realizar commit de los cambios realizados y Pull Request contra master