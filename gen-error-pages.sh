#!/bin/bash

for CODE in 403 404 500 503 555
do
  wget -O varnish/error${CODE}.html "https://www.pagina12.com.ar/error/${CODE}"
done
